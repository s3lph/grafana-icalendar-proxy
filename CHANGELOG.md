# grafana-icalendar-proxy Changelog

<!-- BEGIN RELEASE v0.1 -->
## Version 0.1

First pre-1.0 release of grafana-icalendar-proxy.

### Changes

<!-- BEGIN CHANGES 0.1 -->
- Configure multiple scrape sources
- Authorization: HTTP Basic Auth or TLS Client Certificates
- Offer API endpoints /api/v1/query and /api/v1/query_range
- Support simple PromQL label filters
<!-- END CHANGES 0.1 -->

<!-- END RELEASE v0.1 -->
