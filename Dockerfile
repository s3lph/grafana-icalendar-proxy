FROM python:3.7-stretch

RUN useradd -d /home/gip -m gip \
  && apt-get update -qy \
  && apt-get install -y --no-install-recommends sudo build-essential lintian rsync \
  && python3.7 -m pip install coverage wheel pycodestyle mypy

WORKDIR /home/gip