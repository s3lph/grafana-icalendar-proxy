#!/usr/bin/env python3

from setuptools import setup, find_packages

from grafana_icalendar_proxy import __version__

setup(
    name='grafana_icalendar_proxy',
    version=__version__,
    author='s3lph',
    author_email='',
    description='',
    license='MIT',
    keywords='grafana,ical,icalendar',
    url='https://gitlab.com/s3lph/grafana-icalendar-proxy',
    packages=find_packages(exclude=['*.test']),
    long_description='',
    python_requires='>=3.6',
    install_requires=[
        'bottle',
        'python-dateutil',
        'icalendar',
        'isodate',
        'jinja2',
        'pytz'
    ],
    entry_points={
        'console_scripts': [
            'grafana-icalendar-proxy = grafana_icalendar_proxy:main'
        ]
    },
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Framework :: Bottle',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: MIT License',
        'Topic :: System :: Monitoring'
    ],
)
