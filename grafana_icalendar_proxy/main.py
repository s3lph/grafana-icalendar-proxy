import sys

import bottle

from grafana_icalendar_proxy.config import load_config, load_default_config, get_config

# Contains decorated bottle handler function for /api/v1/query
# noinspection PyUnresolvedReferences
from grafana_icalendar_proxy.api import grafana


def main():
    if len(sys.argv) == 1:
        load_default_config()
    elif len(sys.argv) == 2:
        load_config(sys.argv[1])
    else:
        print(f'Can only read one config file, got "{" ".join(sys.argv[1:])}"')
        exit(1)
    bottle.run(host=get_config().addr, port=get_config().port)


if __name__ == '__main__':
    main()
